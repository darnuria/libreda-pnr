// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Generate test data.


// pub fn generate_random_circuit(
//     num_cells: usize,
//     seed: usize,
// ) -> db::RcNetlist {
//     unimplemented!()
// }