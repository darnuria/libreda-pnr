// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Collection of utility functions.

pub mod pin_detection;
pub mod netlist_validation;
pub mod tie_cell_insertion;
pub mod buffer_removal;