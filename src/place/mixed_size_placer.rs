// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Trait definitions for mixed-size placement algorithms.
//! Mixed-size placers find positions for macro block and standard-cells.


pub use libreda_db::prelude::{SInt, UInt, WindingNumber, TryCastCoord};
pub use libreda_db::prelude as db;
use db::L2NBase;

pub use libreda_db::iron_shapes::prelude::{Point, SimpleRPolygon};
use std::collections::HashMap;
use super::placement_problem::PlacementProblem;

/// Error type used for global placement.
#[derive(Debug)]
pub enum PlacementError {
    /// Stopped placement algorithm because the maximal number of iterations was reached.
    ReachedMaxIterations,
    /// The solution diverges. This is likely a bug in the placement algorithm.
    Divergence,
    /// Other type of error. Defined by an error message string.
    Other(String),
}

impl std::fmt::Display for PlacementError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PlacementError::ReachedMaxIterations => write!(f, "Reached maximum number of iterations."),
            PlacementError::Divergence => write!(f, "Placement diverges."),
            PlacementError::Other(s) => write!(f, "{}", s)
        }
    }
}

impl std::error::Error for PlacementError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
}

/// Interface definition for mixed-size placement engines (for macro blocks and standard cells).
pub trait MixedSizePlacer<C: L2NBase> {
    /// Get the name of the placement engine.
    fn name(&self) -> &str;

    /// Actual implementation of [`MixedSizePlacer::find_cell_positions`].
    fn find_cell_positions_impl(
        &self,
        placement_problem: &dyn PlacementProblem<C>
    ) -> Result<HashMap<C::CellInstId, db::SimpleTransform<C::Coord>>, PlacementError>;

    /// Find the positions of all circuit instances inside `circuit`.
    ///
    /// # Parameters
    ///
    /// * `placement_problem`: [`PlacementProblem`] trait object. This object bundles the netlist, layout and
    /// other parameters relevant for placement such as cell-sizes, fixed/movable cells, net weights.
    ///
    /// # Returns
    ///
    /// Returns a `HashMap` which maps circuit instances to positions.
    fn find_cell_positions(
        &self,
        placement_problem: &dyn PlacementProblem<C>
    ) -> Result<HashMap<C::CellInstId, db::SimpleTransform<C::Coord>>, PlacementError> {

        self.find_cell_positions_impl(placement_problem)

    }
}