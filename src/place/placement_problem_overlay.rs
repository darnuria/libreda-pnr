// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Incremental updates for a [`PlacementProblem`].
//!

pub use libreda_db::prelude as db;
use std::collections::HashMap;
use crate::place::placement_problem::*;

/// Overlay data for [`PlacementProblem`]s.
/// This struct takes a [`PlacementProblem`] and stores updates of initial positions, placement status
/// and net weights. All values which where not set to new values default back to the underlying
/// [`PlacementProblem`].
/// This allows to update such values of a immutable [`PlacementProblem`] with no cloning.
pub struct PlacementProblemOverlay<'a, C: db::L2NBase> {
    /// Underlying placement problem.
    pub placement_problem: &'a dyn PlacementProblem<C>,
    /// Updates for initial positions.
    pub initial_positions: HashMap<C::CellInstId, db::SimpleTransform<C::Coord>>,
    /// Updates for placement status.
    pub placement_status: HashMap<C::CellInstId, PlacementStatus>,
    /// Updates for net weights.
    pub net_weights: HashMap<C::NetId, f64>,
}

impl<'a, C: db::L2NBase> PlacementProblemOverlay<'a, C> {
    /// Create a new overlay over `placement_problem` without any updates.
    /// The returned placement problem will return the same values but they can
    /// be updated without changing the underlying values.
    pub fn new(placement_problem: &'a dyn PlacementProblem<C>) -> Self {
        Self {
            placement_problem,
            initial_positions: Default::default(),
            placement_status: Default::default(),
            net_weights: Default::default()
        }
    }
}

/// Representation of the placement task.
impl<'a, C: db::L2NBase> PlacementProblem<C> for PlacementProblemOverlay<'a, C> {
    fn fused_layout_netlist(&self) -> &C {
        self.placement_problem.fused_layout_netlist()
    }

    fn top_cell(&self) -> C::CellId {
        self.placement_problem.top_cell()
    }

    fn placement_region(&self) -> Vec<db::SimpleRPolygon<C::Coord>> {
        self.placement_problem.placement_region()
    }

    fn soft_blockages(&self) -> Vec<db::SimpleRPolygon<C::Coord>> {
        self.placement_problem.soft_blockages()
    }

    fn initial_position(&self, cell_instance: &C::CellInstId) -> db::SimpleTransform<C::Coord> {
        self.initial_positions.get(cell_instance)
            .cloned()
            // Default to the underlying values.
            .unwrap_or_else(|| self.placement_problem.initial_position(cell_instance))
    }

    fn placement_status(&self, cell_instance: &C::CellInstId) -> PlacementStatus {
        self.placement_status.get(cell_instance)
            .copied()
            .unwrap_or_else(|| self.placement_problem.placement_status(cell_instance))
    }

    fn cell_outline(&self, cell: &C::CellId) -> Option<db::Rect<C::Coord>> {
        self.placement_problem.cell_outline(cell)
    }

    fn net_weight(&self, net: &C::NetId) -> f64 {
        self.net_weights.get(net)
            .copied()
            .unwrap_or_else(|| self.placement_problem.net_weight(net))
    }
}

