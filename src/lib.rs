// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! ASIC place-and-route framework.
//!
//! This crate contains interface definitions for place-and-route related algorithms.
//!
//! The core idea of the framework is to enable independent development of place & route engines
//! which then can easily be plugged together.
//!
//! ## Overview
//! Incomplete overview:
//! * [`place`] - interfaces for placement engines and representations of placement problems
//! * [`rebuffer`] - interface for buffer insertion engines
//! * [`route`] - interfaces for routing engines
//! * [`timing_analysis`] - interfaces for static timing-analysis (STA) engines
//! * [`util`] - useful functions which don't yet have their own category
//!
//! # Implementations
//!
//! Implementations of place and route algorithms are not included in this crate.
//! Some can be found in the following list:
//!
//! ## Placement and legalizaton
//! * [electron-placer](https://codeberg.org/LibrEDA/electron-placer) (written in Rust)
//! * [Coloquinte](https://codeberg.org/tok/libreda-coloquinte-placer) (C bindings, statically linked)
//!
//! ## Routing
//! * [TritonRoute](https://codeberg.org/LibrEDA/libreda-triton-route) (subprocess called by passing LEF/DEF files)
//! * [mycelium](https://codeberg.org/LibrEDA/mycelium-router) (written in Rust)

#![deny(missing_docs)]

/// Re-exports.
pub use libreda_db;
pub use libreda_db::prelude as db;

pub mod design;

pub mod place;
pub mod rebuffer;
pub mod legalize;
pub mod route;
pub mod util;
pub mod metrics;
pub mod timing_analysis;
mod test_data;
