// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Metrics and statistics for evaluating placement & routing quality.
//!
pub mod wirelength_estimation;
pub mod placement_density;
// pub mod density_map;