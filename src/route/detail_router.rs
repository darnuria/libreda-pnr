// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Basic trait for a router.

use crate::db;
use crate::route::routing_problem::DetailRoutingProblem;


/// Basic trait for a detail router.
pub trait DetailRouter<LN: db::L2NBase> {
    /// Result of the detail routing.
    /// The exact type depends on the routing algorithm. The only constraint is
    /// that the result can be drawn to a layout.
    type RoutingResult: DrawDetailRoutes<LN>;
    /// Failure during routing.
    type Error;

    /// Get the name of the routing engine.
    fn name(&self) -> String;

    /// Compute the detail routes.
    /// Neither layout nor netlist are modified. However, a successful result can
    /// be written to the layout.
    fn route<RP>(&self,
                 routing_problem: RP,
    ) -> Result<Self::RoutingResult, Self::Error>
        where RP: DetailRoutingProblem<LN>;
}

/// Trait for routing results which can be drawn to a layout.
pub trait DrawDetailRoutes<LN: db::L2NBase> {
    /// Failure during drawing of the routes.
    type Error;
    /// Draw the detail routes to a layout/netlist.
    fn draw_detail_routes(&self, chip: &mut LN, top: &LN::CellId) -> Result<(), Self::Error>;
}
