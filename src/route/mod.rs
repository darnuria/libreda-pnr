// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Interface definitions for routing related algorithms.

pub mod detail_router;
pub mod simple_router;
pub mod airwire_router;
pub mod global_router;
pub mod routing_problem;
pub mod prelude;