// Copyright (c) 2022-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Interface definitions for global routers.



//! Basic trait for a router.

use crate::db;
use super::routing_problem::GlobalRoutingProblem;


/// Basic trait for a global router.
pub trait GlobalRouter<LN: db::L2NBase> {
    /// Result of the global routing.
    /// The exact type depends on the routing algorithm.
    type RoutingResult;
    /// Failure during routing.
    type Error;

    /// Get the name of the routing engine.
    fn name(&self) -> String;

    /// Compute the global routes.
    /// Neither layout nor netlist are modified.
    fn route<RP>(&self,
                 routing_problem: &RP,
    ) -> Result<Self::RoutingResult, Self::Error>
        where RP: GlobalRoutingProblem<LN>;
}