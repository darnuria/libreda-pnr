// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Trait definitions for re-buffering of high-fanout nets.
//! This includes clock-tree and buffer-tree generators.

pub mod buffer_insertion;