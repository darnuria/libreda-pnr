// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Basic interface for accessing the result of static timing analysis (STA).

use num_traits::Num;
use crate::db::{NetlistBase, TerminalId};

/// Query arrival times.
/// This trait is typically implemented by the result of a static timing analysis step.
/// The type of analysis (early/late) is implicit and not defined by this trait.
pub trait ArrivalTimeQuery<N: NetlistBase> {

    /// Type for representing arrival times. Typically a floating point type.
    type Time: Copy + Num;

    /// Compute the actual arrival time of a signal at the `node`.
    fn actual_arrival_time(&self, node: &TerminalId<N>) -> Self::Time;

    /// Compute the required arrival time of a signal at the `node`.
    fn required_arrival_time(&self, node: &TerminalId<N>) -> Self::Time;

    /// Compute the 'slack' at a terminal.
    /// The slack is the difference `required_arrival_time - actual_arrival_time`.
    /// A negative slack means that the signal arrives later than it should.
    fn slack(&self, node: &TerminalId<N>) -> Self::Time {
        let aat = self.actual_arrival_time(node);
        let rat = self.required_arrival_time(node);
        rat - aat
    }
}